# Batalha Naval 2.0

### Introcução

&emsp;  &emsp; O "Batalha Naval 2.0" é um jogo *single-player* inspirado no famoso jogo de tabuleiro "Batalha Naval". O objetivo deste jogo é descobrir onde as embaracações foram encondidas no tabuleiro dentro do tempo máximo de 5 minutos, equanto o jogador possuir créditos.  A interface gráfica foi desenvolvida para ser fácil e intuitiva para o jogador.

#### Execução
Para executar o jogo é necessário ter instalado no computador o software Eclipse.
Faça o git clone deste repositório para sua máquina. Vá para o terminal e digite o comando:
```
$ git clone https://gitlab.com/rogeriojunior/EP2_OO.git
```
Com o Eclipse aberto :
I) Vá em *FILE* (Arquivo);  
II) Selecione *Open projects from file System…*(Abrir o projeto do sistema de arquivos..);  
III) Na caixa aberta, clique no botão *Directory* e selecione a pasta do jogo salvo no computador;  
IV) Selecione o projeto na barra *Package Explorer*;  
V) Aperte com o botão direito em cima do projeto aberto e selecione a opção *Run As* e em seguida *1 Java Aplication*;
VI) Selecione o arquivo "BatalhaNaval.main" e pressione "Ok".

\*As instruções do jogo estão no menu "Instruções"

*Designed by: Rogério Júnior - 17/0021751*
