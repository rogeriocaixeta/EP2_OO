package game;

import java.util.ArrayList;
import java.util.Vector;

@SuppressWarnings("unused")
public class Board {
	private int [][] matrix;
	private int lines;
	private int columns;
	private Vector<ArrayList<Watercraft>> boats = new Vector<ArrayList<Watercraft>>();
	
	public Board(String path) {
		GameFile file = new GameFile (path);
		
		setLines(file.line);
		setColumns(file.column);
		setMatrix(file.board);
		setBoats(file.boats);
	
	}

	public int getLines() {
		return lines;
	}

	private void setLines(int lines) {
		this.lines = lines;
	}

	public int getColumns() {
		return columns;
	}

	private void setColumns(int columns) {
		this.columns = columns;
	}

	public int getMatrix(int x, int y) {
		return matrix[x][y];
	}

	private void setMatrix(int[][] matrix) {
		this.matrix = matrix;
	}
	
	public ArrayList<Watercraft> getBoats(int position) {
		//Return the Array of boats of the same type
		return boats.get(position);
	}

	private void setBoats(int [] num) {
		int [][] usedLocations = new int [lines][columns]; 
		int boatType, line, row, pitch;
		
		for (int i = 0; i < lines; i++) {
			for (int j = 0; j < columns; j++) {
				usedLocations[i][j] = matrix[i][j];
			}
		}
		
		//Creating each type of boat, according to their quantity
		for (boatType = 0; boatType < 5; boatType++) {
			
			if (num[boatType] != 0) {
				ArrayList<Watercraft> ship = new ArrayList<Watercraft>();
				//Search on the board for the location of each boat
				for (line = 0; line < lines; line++) {
					boolean changeDirection = false;
					
					for (row = 0; row < columns; row++) {
						int [] loc = new int [(boatType+1)*2];
						//Looking for the first occurrence of a boat
						if (matrix[line][row] == (boatType+1) && usedLocations[line][row] != 9) {
							
							if ((line+boatType) < lines) {
								//Looking for the last occurrence of the boat
								
								if (matrix[line+boatType][row] == (boatType+1)) {
									int pos = 0;
									//Getting all the interval of the boat
									for (pitch = 0; pitch < (boatType+1); pitch++) {
										loc[pos] = line + pitch;
										loc[pos + 1] = row;										
										usedLocations[line+pitch][row] = 9; //Not to get the same boat again
										
										pos+=2;
									}
									Watercraft aux_boat = new Watercraft((boatType+1), loc);
									ship.add(aux_boat);
								}
								else {
									//Not in the same direction
									changeDirection = true;
								}
								
							}
							
							else {
								//Out of lines
								changeDirection = true;
							}
							
							if ((row+boatType) < columns && changeDirection) {
								
								if (matrix[line][row+boatType] == (boatType+1)) {
									int pos = 0;
									for (pitch = 0; pitch < (boatType+1); pitch++) {
										loc[pos] = line;
										loc[pos + 1] = row + pitch;										
										usedLocations[line][row+pitch] = 9;
										
										pos+=2;
									}
									Watercraft aux_boat = new Watercraft((boatType+1), loc);
									ship.add(aux_boat);
								}
								changeDirection = false;
							}
							
													
						}
					}
				}
				
				boats.addElement(ship);
			}
		}
		
	}

	public int [] getBoatLocation(int type, int number) {
		return boats.get(type - 1).get(number - 1).getLocation();
	}
	
	public boolean newMovement(int x, int y) {
		int i,j;
		
		for (i = 0; i < boats.size(); i++) {
			for (j = 0; j < getBoats(i).size(); j++) {
				if (getBoats(i).get(j).newDiscover(x, y)) {
					return true;
				}			
			}
		}
		
		return false;
	}
	
	public boolean somethingHere(int line, int column) {
		if (getMatrix(line, column) != 0) {
			return true;
		}
		return false;
	}
	
	public int getDiscoveredBoats() {
		int i,j, quantity = 0;
		
		for (i = 0; i < boats.size(); i++) {
			for (j = 0; j < getBoats(i).size(); j++) {
				if (getBoats(i).get(j).getState()) {
					quantity ++;
				}			
			}
		}
		
		return quantity;
	}

	public int totalOfBoats() {
		int i,j, total = 0;
		
		for (i = 0; i < boats.size(); i++) {
			total += getBoats(i).size();
		}
		
		return total;
	}
	
	public Vector<ArrayList<Watercraft>> getBoats() {
		//Return all the Array
		return boats;
	}
}
