package game;

import java.util.Vector;

@SuppressWarnings("unused")
public class Player {
	private int credits;
	private int points;
	public static final int timeLimit = 300;
	private Board gameBoard;
	
	public Player(Board gameBoard){
		this.gameBoard = gameBoard;
		credits = 0;		
		
		for (int i = 0; i <gameBoard.getBoats().size(); i++) {
			for (int j = 0; j < gameBoard.getBoats(i).size(); j++) {
				credits += (1 * gameBoard.getBoats(i).get(j).getType());
			}
		}
			
		credits += 10;
		points = 0;
	}
	
	public boolean singleAttack(int x, int y) {
		if (credits >= 1) {
		
			if (gameBoard.newMovement(x, y)) {
				points+=2;
				return true;
			}
			else {	
				//Punishment
				if (points >= 3) {
					points -= 3;
					credits -= 1;
				}
				return false;
			}
		}
		
		return false;
	}
	
	public boolean discoverBoats(Vector<Integer> range) {
		if (credits >= 3) {
			credits -= 3;
			for (int i = 0; i < range.size(); i+=2) {
				if ( gameBoard.somethingHere(range.get(i), range.get(i+1) )) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public Vector<Integer> multipleAttacks(Vector<Integer> range) {
		Vector<Integer> attackedPositions = new Vector <Integer>();
		
		if (credits >= 5) {
			credits -= 5;
			int numberOfAttacks = 0;
		
			for (int i = 0; i < range.size(); i+=2) {
				if ( gameBoard.newMovement(range.get(i), range.get(i+1)) ) {
					attackedPositions.add(range.get(i));
					attackedPositions.add(range.get(i+1));
				}
				else {
					//Punishment
					if (points >= 3) {
						points -= 3;
					}
				}
			}
		}
		
		points += attackedPositions.size();
		return attackedPositions;
	}
	
	public Vector<Integer> specialAttack(Vector<Integer> range) {
		Vector<Integer> attackedPositions = new Vector <Integer>();
		
		if(credits >= 10) {
			credits -=10;
			int numberOfAttacks = 0;
		
			for (int i = 0; i < range.size(); i+=2) {
				if ( gameBoard.newMovement(range.get(i), range.get(i+1)) ) {
					attackedPositions.add(range.get(i));
					attackedPositions.add(range.get(i+1));
				}
				else {
					//Punishment
					if (points >= 3) {
						points -= 3;
					}
				}
			}
		}
		
		points += attackedPositions.size();
		return attackedPositions;
	}

	public int getCredits() {
		return credits;
	}

	public int getPoints() {
		return points;
	}

}
