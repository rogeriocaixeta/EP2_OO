package game;

public class Watercraft {
	private int type;
	private int [] location;
	private int [] discovered_location;
	private boolean state = false;
	private int [] discovered;
	
	
	public Watercraft(int type, int[] location) {
		setType(type);
		this.location = new int [this.type * 2]; //To keep all the positions (x,y) of the Watercfart
		discovered_location = new int [this.type * 2];
		discovered = new int [this.type]; 
				
		for (int i = 0; i < this.type; i++) {
			discovered[i] = 0;
		}
		
		for (int i = 0; i < this.type * 2; i+=2) {
			discovered_location[i] = 0;
			discovered_location[i+1] = 0;
		}
		
		
		setLocation(location);
	}
	
	
	public int getType() {
		return type;
	}
	public void setType(int size) {
		this.type = size;
	}	
	public int [] getLocation() {
		return location;
	}
	public int getLocation(int x) {
		return location[x];
	}
	public void setLocation(int[] location) {
		this.location = location;
	}
	

	public boolean newDiscover(int column, int line) {
		for (int i = 0; i < type*2; i+=2){
			if ((location[i] == column && location[i+1] == line) && (discovered_location[i] == 0 && discovered_location[i+1] == 0)) {
				discovered[i/2] = 1;
				discovered_location[i] = -1;
				discovered_location[i+1] = -1;
				return true;
			}
		}
		return false;
	}
		
	public boolean getState() {
		state = true;
		
		for (int i = 0; i < type; i++) {
			if (discovered[i] != 1) {
				state = false;
			}
		}
		
		return state;
	}
		
}
