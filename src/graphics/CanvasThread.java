package graphics;

import game.*;

public class CanvasThread extends Thread {
	private CanvasGame canvas;
	private Board board;
	private boolean running = true; 
	
	public CanvasThread(CanvasGame canvas, Board board) {
		this.canvas = canvas;
		this.board = board;
	}
	
	@Override
	public void run() {
		while(running) {
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			canvas.paint(canvas.getGraphics(), board);
		}
	}

}
