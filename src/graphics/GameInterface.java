package graphics;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Vector;
import game.*;

@SuppressWarnings("serial")
public class GameInterface extends JFrame {
	private JFrame menuFrame;
	private JFrame gameFrame;
	private JFrame instructionsFrame;
		
	private JPanel menu;
	private JPanel game;
	private JPanel instructions;
	
	private JLabel time;
	private JLabel credits;
	private JLabel points;
	private JLabel messages;
	
	public int contador;

	
	private Font gameFont;
	private Font buttonFont;
	private Font titleFont;
	private CanvasGame canvas;
	private Board board;
	private Player player;
	
	public GameInterface (Board board) {		
		player = new Player(board);
		canvas = new CanvasGame(board);
		this.board = board;
		
		setFont();
		setMenuFrame();
		setGameFrame();
		setInstructionsFrame();
				
		menuFrame.setVisible(true);
		gameFrame.setVisible(false);
		instructionsFrame.setVisible(false);
	}
	
	private void setMenuFrame() {
		ButtonClickAction action = new ButtonClickAction();
		GameTimer timer = new GameTimer();
		
		JLabel title = new JLabel("<html><head></head><body><font color=\"blue\">Batalha Naval 2.0</font></body></html>", JLabel.CENTER);
		title.setFont(titleFont);
				
		//Menu's Buttons Declaration
		JButton playBT = new JButton("Jogar");
		playBT.setFont(buttonFont);
		playBT.setActionCommand("Play");
		playBT.addActionListener(action);
		playBT.addActionListener(timer);
		
		JButton instructionBT = new JButton("Intruções");
		instructionBT.setFont(buttonFont);
		instructionBT.setActionCommand("Instructions");
		instructionBT.addActionListener(action);
		
		JButton outBT = new JButton("Sair");
		outBT.setFont(buttonFont);
		outBT.setActionCommand("Exit");
		outBT.addActionListener(action);
		//End of Button Declarations
		
		//Panel's Declarations
		JPanel menu = new JPanel(new GridLayout(3,1));
		menu.add(playBT);
		menu.add(instructionBT);
		menu.add(outBT);
		
		JPanel header = new JPanel(new GridLayout());
		header.add(title);
		//End of Panel's Declaration
		
		//General Panel
		this.menu = new JPanel(new BorderLayout());
		this.menu.add(header, BorderLayout.NORTH);
		this.menu.add(menu, BorderLayout.CENTER);
		//End of General Panel
		
		menuFrame = new JFrame("Batalha Naval");
		menuFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menuFrame.setSize(700, 700);
		menuFrame.setLayout(new BorderLayout());
		menuFrame.setLocationRelativeTo(null);
		
		menuFrame.add(this.menu);		
	}
	
	private void setGameFrame() {	
		GameActions action = new GameActions();
		
		//Set of Label's
		messages = new JLabel("Batalha Naval 2.0 - Designed By: Rogério Júnior", JLabel.CENTER);
		messages.setFont(new Font("Verdana", 0, 19));
		
		points = new JLabel("Pontuação: " + String.valueOf(player.getPoints()), JLabel.LEFT);
		points.setFont(gameFont);
		
		credits = new JLabel("Créditos: " + String.valueOf(player.getCredits()), JLabel.CENTER);
		credits.setFont(gameFont);
		
		time = new JLabel("", JLabel.RIGHT);
		time.setFont(gameFont);
		// End of Components
		
		//Set of Buttons Movements
		JButton singleBT = new JButton("Ataque Simples");
		singleBT.setFont(buttonFont);
		singleBT.setActionCommand("Single Attack");
		singleBT.addActionListener(action);
		
		JButton discoverBT = new JButton("Descobrir Barcos");
		discoverBT.setFont(buttonFont);
		discoverBT.setActionCommand("Discover Boats");
		discoverBT.addActionListener(action);
		
		JButton multiple_attackBT = new JButton("Ataque Multiplo");
		multiple_attackBT.setFont(buttonFont);
		multiple_attackBT.setActionCommand("Multiple Attack");
		multiple_attackBT.addActionListener(action);
		
		JButton special_attackBT = new JButton("Ataque Especial");
		special_attackBT.setFont(buttonFont);
		special_attackBT.setActionCommand("Special Attack");
		special_attackBT.addActionListener(action);
		//End of Buttons"Tempo: "
		
		//Set of Canvas
		canvas.addMouseListener(action);
		//End of Canvas

		//Set of Panels
		JPanel gameInformations = new JPanel(new GridLayout(1,3));
		gameInformations.add(points);
		gameInformations.add(credits);
		gameInformations.add(time);
		
		JPanel grid = new JPanel (new BorderLayout());
		grid.add(canvas, BorderLayout.CENTER);
		
		JPanel movements = new JPanel (new GridLayout(4,1));
		movements.add(singleBT);
		movements.add(discoverBT);
		movements.add(multiple_attackBT);
		movements.add(special_attackBT);
		
		JPanel comunicator = new JPanel(new BorderLayout());
		comunicator.add(messages, BorderLayout.CENTER);
		
		JPanel space = new JPanel ();
		space.setPreferredSize(new Dimension(20, 0));
		//End of Panels
		
		//Set general Pane
		game = new JPanel(new BorderLayout(0,20));
		game.add(gameInformations, BorderLayout.NORTH);
		game.add(movements, BorderLayout.WEST);
		game.add(space, BorderLayout.EAST);
		game.add(comunicator, BorderLayout.SOUTH);
		game.add(grid, BorderLayout.CENTER);
		//End of General Pane
				
		gameFrame = new JFrame("Batalha Naval");
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameFrame.setSize(board.getColumns() * (CanvasGame.squareSize + CanvasGame.margin + 37), board.getLines() * (CanvasGame.squareSize + CanvasGame.margin + 15) );
		gameFrame.setLayout(new BorderLayout());
		gameFrame.setLocationRelativeTo(null);
		
		gameFrame.add(game);
	}
	
	private void setInstructionsFrame() {
		ButtonClickAction action = new ButtonClickAction();
		String titleText = "<html><head></head><body><font color=\"blue\">Instruções do Jogo Batalha Naval 2.0</font></body></html>";
		
		JLabel title = new JLabel(titleText, JLabel.CENTER);
		title.setFont(titleFont);
		JLabel text = new JLabel(setInstructionsText(), JLabel.CENTER);
		text.setFont(gameFont);
		
		JButton returnBT = new JButton("Retornar ao Menu Inicial");
		returnBT.setFont(buttonFont);
		returnBT.setActionCommand("Return");
		returnBT.addActionListener(action);
		
		instructions = new JPanel(new BorderLayout());
		instructions.add(title, BorderLayout.NORTH);
		instructions.add(text, BorderLayout.CENTER);
		instructions.add(returnBT, BorderLayout.SOUTH);
		
		
		instructionsFrame = new JFrame("Batalha Naval");
		instructionsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		instructionsFrame.setSize(700, 700);
		instructionsFrame.setLayout(new BorderLayout());
		instructionsFrame.setLocationRelativeTo(null);	
		
		instructionsFrame.add(instructions, BorderLayout.CENTER);
		
	}
	
	private void setFont() {
		gameFont = new Font("Verdana", 0, 18);
		buttonFont = new Font("Verdana", 1, 18);
		titleFont = new Font("Verdana", 1, 20);
	}
	
	private String setInstructionsText() {
		String text = new String();
		
		text = "<html>"
				+ "<head>"
				+ "</head>"
				+ "<body>"
				+ "<p> Este jogo consiste em um jogo single-player cujo o objetivo é descobrir a posição de todos os barcos do tabuleiro. O jogador poderá jogar enquanto tiver créditos e não ultrapassar o tempo limite de 5 minutos. "
				+ "<br> Para usar os vários tipos de jogada basta clicar no respectivo botão, de forma que: <ul> <li>Ataque Simples (2 créditos): o jogador deve selecionar uma posição do tabuleiro para atacar; </li> <li> Descobrir Barcos (3 créditos): o jogador deve selecionar 4 posições do tabuleiro e caso haja alguma embarcação nessas posições aparecerá uma mensagem informativa no rodapé; </li> <li> Ataque Multiplo (5 créditos): o jogador deve selecionar 4 posições do tabuleiro para atacar; </li> <li> Ataque Especial (10 créditos): este é um ataque de linha/coluna. O jogador deve selecionar o inicio e o fim da linha/coluna que pretende atacar. </li> </ul> "
				+ "<br> Os ícones representam: <ul> <li> Verde: a localização de um barco foi encontrada; </li> <li> Fumaça: Um tiro na água; </li> <li> Preto: um barco foi encontrado. </li> </ul> "
				+ "<br> Caso o jogador descubra todos os barcos dentro do tempo estabelecido e antes de esgotar os créditos, ganhará o jogo! "
				+ "</p>"
				+ "</body>"
				+ "</html>";
		
		return text;
	}
	//Actions Implementations
	private class ButtonClickAction implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent action) {
			String command = action.getActionCommand();
			
			if (command.equals("Play")) {
				menuFrame.setVisible(false);
				gameFrame.setVisible(true);
				instructionsFrame.setVisible(false);
				
				CanvasThread updateScreen = new CanvasThread(canvas, board);
				updateScreen.start();
			}
			else if (command.equals("Instructions")) {
				menuFrame.setVisible(false);
				gameFrame.setVisible(false);
				instructionsFrame.setVisible(true);
			}
			else if (command.equals("Return")) {
				menuFrame.setVisible(true);
				gameFrame.setVisible(false);
				instructionsFrame.setVisible(false);
			}
			else if (command.equals("Exit")) {
				System.exit(0);
			}			
		}
		
	}
	
	private class GameActions implements MouseListener, ActionListener{
		private int option;
		private Vector <Integer> selectedPositions;
		
		public GameActions () {
			option = 0;
			selectedPositions = new Vector <Integer>();
		}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			int x = e.getX();
			int y = e.getY();
			
			int x_pos = y/(CanvasGame.squareSize + CanvasGame.margin);
	        int y_pos = x/(CanvasGame.squareSize + CanvasGame.margin);
	        
	        if (x_pos < board.getLines() && y_pos < board.getColumns()) {
	        	if (option == 1){
	        		if (player.singleAttack(x_pos, y_pos)){
	        			canvas.setShot(x_pos, y_pos);
	        		}
	        		else {
	        			canvas.setWaterShot(x_pos, y_pos);
	        		}
	        		option = 0;
	        		messages.setText("Batalha Naval 2.0 - Designed By: Rogério Júnior");
	        	}
	        	else if (option == 2){
	        		selectedPositions.add (x_pos);
	        		selectedPositions.add (y_pos);
	        		
	        		if (selectedPositions.size() == 8){
	        			if (player.discoverBoats(selectedPositions)){
	        				messages.setText("Tem barco(s) nessa área!");
	        			}

	        			option = 0;
	        			selectedPositions.clear();
	        		}

	        	}
	        	else if (option == 3){
	        		selectedPositions.add (x_pos);
	        		selectedPositions.add (y_pos);

	        		if (selectedPositions.size() == 8){
	        			Vector<Integer> range = player.multipleAttacks(selectedPositions);
	        			//Set all the clicks as Water Shot
	        			for (int i = 0; i < selectedPositions.size(); i+=2) {
	        				canvas.setWaterShot(selectedPositions.get(i), selectedPositions.get(i+1));
	        			}
	        			//Set the founded boats as a Right Shot
	        			for (int i = 0; i < range.size(); i+=2) {
	        				canvas.setShot(range.get(i), range.get(i+1));
	        			}
	        		
	        			option = 0;
	        			selectedPositions.clear();
	        			messages.setText("Batalha Naval 2.0 - Designed By: Rogério Júnior");
	        		}
	        	}
	        	else if (option == 4){
	        		selectedPositions.add (x_pos);
	        		selectedPositions.add (y_pos);

	        		if (selectedPositions.size() == 4){
	        			Vector<Integer> interval = new Vector<Integer>();
	        			Vector<Integer> attacked = new Vector<Integer>();

	        			if (selectedPositions.get(0) == selectedPositions.get(2)){
	        				for (int i = 0; i < board.getColumns(); i++){
	        					interval.add(selectedPositions.get(0));
	        					interval.add(i);
	        				}
	        			}
	        		
	        			else if (selectedPositions.get(1) == selectedPositions.get(3)){
	        				for (int i = 0; i < board.getLines(); i++){
	        					interval.add(i);
	        					interval.add(selectedPositions.get(1));
	        				}
	        			}

	        			attacked = player.specialAttack(interval);
	        			//Set all the clicks as Water Shot
	        			for (int i = 0; i < interval.size(); i+=2) {
	        				canvas.setWaterShot(interval.get(i), interval.get(i+1));
	        			}
	        			//Set the founded boats as a Right Shot
	        			for (int i = 0; i < attacked.size(); i+=2) {
	        				canvas.setShot(attacked.get(i), attacked.get(i+1));
	        			}

	        			option = 0;
	        			selectedPositions.clear();
	        			messages.setText("Batalha Naval 2.0 - Designed By: Rogério Júnior");
	        		}
	        	}
			}
	        
	        credits.setText("Créditos: " + String.valueOf(player.getCredits()));
	        points.setText("Pontuação: " + String.valueOf(player.getPoints()));
		}

		@Override
		public void actionPerformed(ActionEvent action) {
			String command = action.getActionCommand();
			
			if (option == 0) {
				messages.setText("Batalha Naval 2.0 - Designed By: Rogério Júnior");
				if (command.equals("Single Attack")) {
					option = 1;
					messages.setText("Selecione uma posição do tabuleiro!");
				}
				else if (command.equals("Discover Boats")) {
					option = 2;
					messages.setText("Selecione quatro posições do tabuleiro para achar um barco nessas posições!");
				}
				else if (command.equals("Multiple Attack")) {
					option = 3;
					messages.setText("Selecione quatro posições do tabuleiro para atacar os barcos dessas posições!");
				}
				else if (command.equals("Special Attack")) {
					option = 4;	
					messages.setText("Selecione o início e o fim da linha/coluna para atacar toda essas posições!");
				}
			}
			
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {			
		}

		@Override
		public void mousePressed(MouseEvent e) {			
		}

	}
	
	private class GameTimer implements ActionListener{
		
	private int seconds;
	private int minutes;

		@Override
		public void actionPerformed(ActionEvent evt) {                                         
	        Thread timerThread = new Thread(new Runnable() {
	            public void run() {
	                 try {
	                    for(contador=0;contador<=600;contador++){
	                    	seconds = contador%60;
	                    	minutes = contador/60;
	                    	
	                        time.setText("Tempo: "+ String.format("%02d:%02d", minutes, seconds));
	                        
	                        // Conditions of Lose
	                        if (contador == Player.timeLimit || player.getCredits() == 0) {
	            	        	menuFrame.setVisible(true);
	            				gameFrame.setVisible(false);
	            				
	            				JOptionPane.showMessageDialog(null, "Você Perdeu!!", "End Game", JOptionPane.ERROR_MESSAGE);
	            				break;
	            	        }
	            	        //Condition of Win
	            	        if (board.getDiscoveredBoats() == board.totalOfBoats()) {
	            	        	menuFrame.setVisible(true);
	            				gameFrame.setVisible(false);
	            				
	            				JOptionPane.showMessageDialog(null, "Você Ganhou!!", "Win Game", JOptionPane.INFORMATION_MESSAGE);
	            				break;
	            	        }
	                        
	                        Thread.sleep(1000);
	                    }
	                 } catch (InterruptedException ex) {}
	             }
	        });
	        timerThread.start();
		}
		
	}
	
}
