package main;

import java.util.Random;

import game.*;
import graphics.*;

public class BatalhaNaval {

	public static void main(String[] args) {
		Random number = new Random();
		
		Board gameBoard = new Board ("maps/map_" + String.valueOf(1 + number.nextInt(4)) + ".txt");
				
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            System.err.println(ex);
        } 
		
		@SuppressWarnings("unused")
		GameInterface gameWindow = new GameInterface(gameBoard);
	}

}
