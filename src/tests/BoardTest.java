package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Vector;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game.Board;
import game.Watercraft;

class BoardTest {
	private String path;
	private Board board;
	private int lines, columns;
	private int [][] testMatrix = {{0,0,3,3,3,0},{0,1,0,0,0,0},{0,0,2,0,3,0},{0,0,2,0,3,0},{0,0,0,0,3,0},{2,2,0,0,1,0}};
	private Vector<ArrayList<Watercraft>> testBoats = new Vector<ArrayList<Watercraft>>();
	private int[] location1 = {1,1};
	private int[] location2 = {5,4};
	private int[] location3 = {2,2,3,2};
	private int[] location4 = {5,0,5,1};
	private int[] location5 = {0,2,0,3,0,4};
	private int[] location6 = {2,4,3,4,4,4};
	
	@BeforeEach
	void beforeTests() {
		path = "maps/map_3.txt";
		board = new Board(path);
		
		lines = 6;
		columns = 6;
		
		
		Watercraft auxWatercraft;
		ArrayList<Watercraft> arrayTest1 = new ArrayList<Watercraft>();
		ArrayList<Watercraft> arrayTest2 = new ArrayList<Watercraft>();
		ArrayList<Watercraft> arrayTest3 = new ArrayList<Watercraft>();
		
		
		auxWatercraft = new Watercraft(1, location1);
		arrayTest1.add(auxWatercraft);
		
		
		auxWatercraft = new Watercraft(1, location2);
		arrayTest1.add(auxWatercraft);
		
		testBoats.add(arrayTest1);
		
		
		auxWatercraft = new Watercraft(2, location3);
		arrayTest2.add(auxWatercraft);
		
		
		auxWatercraft = new Watercraft(2, location4);
		arrayTest2.add(auxWatercraft);
		
		testBoats.add(arrayTest2);
		
		
		auxWatercraft = new Watercraft(3, location5);
		arrayTest3.add(auxWatercraft);
		
		
		auxWatercraft = new Watercraft(3, location6);
		arrayTest3.add(auxWatercraft);
		
		testBoats.add(arrayTest3);
	}
	
	@Test
	void testBoard() {						
		for (int i = 0; i < lines; i++) {
			for (int k = 0; k < columns; k++) {
				assertEquals(board.getMatrix(i, k), testMatrix[i][k]);
			}
		}
		
		for (int i = 0; i < board.getBoats().size(); i++) {
			for (int k = 0; k < board.getBoats().get(i).size(); k++) {
				assertEquals(board.getBoats().get(i).get(k).getType(), testBoats.get(i).get(k).getType());
				
				for (int j = 0; j < board.getBoats().get(i).get(k).getLocation().length; j++) {
					assertEquals(board.getBoats().get(i).get(k).getLocation(j), testBoats.get(i).get(k).getLocation(j));
				}
			}
		}
		
		assertEquals(board.getBoats(1).get(0).getLocation(0), testBoats.get(1).get(0).getLocation(0));
		
		assertEquals(board.getLines(), lines);
		assertEquals(board.getColumns(), columns);
	}
	
	@Test
	void testgetBoatLocation() {
		int [] location = new int [2]; 
		location[0] = board.getBoatLocation(1, 1)[0];
		location[1] = board.getBoatLocation(1, 1)[1];
		
		assertEquals(location[0], location1[0]);
		assertEquals(location[1], location1[1]);
	}
	
	@Test
	void testNewMovement() {
		assertEquals(board.newMovement(1, 1), true);
		assertEquals(board.newMovement(0, 0), false);
	}

	@Test
	void testSomethingHere() {
		assertEquals(board.somethingHere(0, 0), false);
		assertEquals(board.somethingHere(1, 1), true);
	}
	
	@Test
	void testGetDiscoveredBoats () {
		Board boardWithDiscover = new Board(path);
		
		boardWithDiscover.newMovement(5, 4);
		
		assertEquals(boardWithDiscover.getDiscoveredBoats(), 1);
		assertEquals(board.getDiscoveredBoats(), 0);
	}
	
	@Test
	void testTotalOfBoats() {
		assertEquals(board.totalOfBoats(), 6);
		
	}
}
